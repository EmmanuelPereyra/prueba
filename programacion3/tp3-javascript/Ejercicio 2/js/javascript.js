/*Completar el código JavaScript proporcionado para que cuando se haga click sobre el enlace se muestre
completo el contenido de texto. Además, el enlace debe dejar de mostrarse después de pulsarlo por
primera vez (al hacer click sobre el enlace se debe ejecutar la función llamada muestra()).*/

function muestra() {
	var span = document.getElementById("adicional");
	span.style.display = "contents";

	var enlace = document.getElementById("enlace");
	enlace.style.display = "none";
}