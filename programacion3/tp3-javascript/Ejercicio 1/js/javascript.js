window.onload = function () {
	//Numero de enlaces de la página
	var etiquetaA = document.getElementsByTagName("a");
	var parrafo = document.createElement("p");
	var contenido = document.createTextNode("Hay " + etiquetaA.length + " enlaces en la página");
	parrafo.appendChild(contenido);
	document.body.appendChild(parrafo);



	//Direccion a la que enlaza el penúltimo enlace
	parrafo = document.createElement("p");
	contenido = document.createTextNode("El penúltimo enlace direcciona a: " + etiquetaA[5].href);
	parrafo.appendChild(contenido);
	document.body.appendChild(parrafo);



	//Numero de enlaces que enlazan a http://prueba/
	var contador = 0;
	for (var i = 0; i < etiquetaA.length; i++) {
		if (etiquetaA[i].href == "http://prueba/") {
			contador++;
		}
	}
	parrafo = document.createElement("p");
	contenido = document.createTextNode("Numero de enlaces que direccionan a http://prueba/ son: " + contador);
	parrafo.appendChild(contenido);
	document.body.appendChild(parrafo);



	//Número de enlaces del tercer párrafo
	var p = document.getElementsByTagName("p");
	contador = p[2].getElementsByTagName("a").length;
	parrafo = document.createElement("p");
	contenido = document.createTextNode("Numero de enlaces del tercer párrafo: " + contador);
	parrafo.appendChild(contenido);
	document.body.appendChild(parrafo);
}