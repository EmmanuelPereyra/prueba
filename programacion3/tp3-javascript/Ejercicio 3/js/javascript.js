/*Completar el código JavaScript proporcionado para que se añadan nuevos elementos a la lista cada vez
que se pulsa sobre el botón. Utilizar las funciones DOM para crear nuevos nodos y añadirlos a la lista
existente (al hacer click sobre el botón se debe ejecutar la función llamada anade()).*/

function anade() {
	var lista = document.getElementById("lista");
	var elementoLi = document.createElement("li");
	var contenido = document.createTextNode("Nuevo elemento añadido");
	elementoLi.appendChild(contenido);
	lista.appendChild(elementoLi);
}